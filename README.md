# Markdown cheatsheet

## Table Of Contents
Tha table of contents only works in GitLab. Use it for Wiki only.

[[_TOC_]]

## Highlighting

### Highlighting text
- Markdown itself does not support colored or highlighted text.
- However, we can use **HTML** within our Markdown, and **CSS** for styling, but **<span style="background-color: gold;">this is not working in GitLab!</span>**

### Inline code
This is a simple inline `code`.

### Code blocks
```html
<meta charset="UTF-8">
```

```js
var myVar = 10;
// JetBrains CE version IDE-s does not provide syntax highlight for every language
```

### Run `shell` commands in any JetBrains IDE
- using the `shell` keyword in our Markdown code (readme.md)
- we can run shell commands by clicking the green forward button (visible only in JetBrains IDE) 

**the code:**

\```shell

node --version

\```

**the result:**

```shell
node --version
```


## Tasks

- [x] Completed task
- [ ] Incomplete task
  - [x] Sub-task 1
  - [ ] Sub-task 3

- [x] Completed task
  - [ ] Incomplete task
    - [ ] Sub-task 1

## Lists
### Ordered list
1. first
2. second
3. etc...

### Unordered list
- item 1
- item 2
- etc...

## Includes (like in AsciiDoc)
- not possible without extra plugins :(

## GitLab Flavored Markdown (GLFM)
https://docs.gitlab.com/ee/user/markdown.html

